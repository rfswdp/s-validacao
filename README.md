# s-validacao #

Script de validação do CNPJ desenvolvido com base no geradordecnpj.

### O que ele faz? ###

* Valida CNPJs - Ignora sequências numéricas como 000 111 222 333 até 9999999999
* Valida CPFs - Ignora sequências numéricas como 000 111 222 até 99999999999


### Como usar? ###

Para validar basta usar a função:
_sCNPJ(var);_ ou _sCPF(var);_
Onde var é a string que você quer validar.
O resultado será TRUE ou FALSE.


### Futuras funções ###

* Validar E-mail