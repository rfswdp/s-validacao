/*
 * Desenvolvido originalmente por: http://www.geradorcnpj.com/ // Apenas script de CNPJ
 * Alterado por: R.Schubert
 * Data: Novembro 2015
 *
 * Versão: 0.1.0
 *
 * ATENÇÃO! Os números de CNPJ e CPF foram gerados aleatoriamente pelas ferramentas fornecidas em:
 * www.geradorcpf.com e www.geradorcnpj.com
 * Não nos responsabilizamos caso os números aleatórios sejam condizentes com números reais.
 * O usuário se torna responável pelo uso ou mal uso das informações aqui disponibilizadas para teste.
 *
 * CNPJs para Teste de validação: 88.814.323/0001-60 | 34.275.854/0001-00 | 48.735.766/0001-40 | 31.731.914/0001-56
 * CPFs para Teste de validação: 372.955.342-96 | 822.615.612-29 | 358.375.256-29 | 899.043.449-14
 * 
 */

function sCNPJ(cnpj) {
 
    cnpj = cnpj.replace(/[^\d]+/g,'');
 
    if(cnpj == '') return false;
     
    if (cnpj.length != 14)
        return false;
 
    // EXCLUIR SEQUENCIAIS DE NÚMERO DA LISTA DE VERIFICAÇÃO
    if (cnpj == "00000000000000" || 
        cnpj == "11111111111111" || 
        cnpj == "22222222222222" || 
        cnpj == "33333333333333" || 
        cnpj == "44444444444444" || 
        cnpj == "55555555555555" || 
        cnpj == "66666666666666" || 
        cnpj == "77777777777777" || 
        cnpj == "88888888888888" || 
        cnpj == "99999999999999")
        return false;
         
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
         
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
          return false;
           
    return true;
    
}

function sCPF(cpf) {
	var Soma;
	var Resto; 
	Soma = 0;

	cpf = cpf.replace(/[^\d]+/g,'');

	if (cpf.length != 11 || 
		cpf == "00000000000" || 
		cpf == "11111111111" || 
		cpf == "22222222222" || 
		cpf == "33333333333" || 
		cpf == "44444444444" || 
		cpf == "55555555555" || 
		cpf == "66666666666" || 
		cpf == "77777777777" || 
		cpf == "88888888888" || 
		cpf == "99999999999") 
		return false;

	for (i=1; i<=9; i++) Soma = Soma + parseInt(cpf.substring(i-1, i)) * (11 - i);
	Resto = (Soma * 10) % 11; 

	if ((Resto == 10) || (Resto == 11)) Resto = 0;
	if (Resto != parseInt(cpf.substring(9, 10))) return false;
	
	Soma = 0; 

	for (i = 1; i <= 10; i++) Soma = Soma + parseInt(cpf.substring(i-1, i)) * (12 - i);
		Resto = (Soma * 10) % 11; 

	if ((Resto == 10) || (Resto == 11)) Resto = 0;
	if (Resto != parseInt(cpf.substring(10, 11) ) ) return false;

	return true;

}